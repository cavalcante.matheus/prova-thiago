#!/bin/bash


data=$(date +"%Y-%m-%d-%H")

mkdir /tmp/$data

cp -r ./* /tmp/$data/

zip -r arquivosdatacompactados.zip /tmp/$data/*

mv arquivosdatacompactados.zip ./

rm -r /tmp/$data

